use challengeone;
DROP PROCEDURE IF EXISTS q1;

DELIMITER //
CREATE PROCEDURE q1 (IN ticker text, IN indate datetime)
BEGIN
SELECT
SUM(vol*close)/SUM(vol) as VWAP,
DATE_FORMAT(`date`, '%d/%m/%Y') as DateOnly, DATE_FORMAT(`date`, '%H:%i') as StartTime,
DATE_FORMAT(DATE_ADD(indate, INTERVAL 5 HOUR), '%H:%i') as EndTime
FROM trade_data
WHERE date BETWEEN indate and DATE_ADD(indate, INTERVAL 5 HOUR);
END //
DELIMITER ;
