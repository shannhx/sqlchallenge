DROP PROCEDURE IF EXISTS q2;

DELIMITER //
CREATE PROCEDURE q2 ()
BEGIN

DROP TEMPORARY TABLE IF EXISTS DaysByLargestRange;
DROP TEMPORARY TABLE IF EXISTS MaxPricePerDate;
DROP TEMPORARY TABLE IF EXISTS TimeAtMaxPrice;
DROP TEMPORARY TABLE IF EXISTS DateRangeTime;

CREATE TEMPORARY TABLE DaysByLargestRange
SELECT date, open, close, abs(`open` - `close`) AS `Range` 
FROM sample_dataset3
ORDER BY `Range` DESC
LIMIT 4;

CREATE TEMPORARY TABLE MaxPricePerDate
SELECT date, MAX(high) AS MaxPrice
FROM sample_dataset3
WHERE sample_dataset3.date IN (SELECT date FROM DaysByLargestRange)
GROUP BY date;

CREATE TEMPORARY TABLE TimeAtMaxPrice
SELECT a.date, time, high
FROM sample_dataset3 AS a INNER JOIN  MaxPricePerDate AS b
ON a.date = b.date AND a.high = b.MaxPrice;

CREATE TEMPORARY TABLE DateRangeTime
SELECT DATE_FORMAT(a.date, "%m/%d/%Y") AS Date, CAST(`Range` AS DECIMAL(10,4)) AS `Range`, b.time as Time_of_Max_Price
FROM DaysByLargestRange AS a INNER JOIN TimeAtMaxPrice AS b
ON a.date = b.date;

SELECT * FROM DateRangeTime;

END //
DELIMITER ;



