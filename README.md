#SQL Challenges

##Challenge One 

After the dataset was downloaded, I used the TableData Import Wizard to import it to MySQL Workbench. This was accessed by right clicking tables in SCHEMAS. The data was then configured from this wizard and date was changed from 'bigint' to 'datetime' data type.

I created a database using CREATE DATABASE challengeone to store this table within.

Firstly, I used the exmaple SQL code provided in the challenge and modified the attributes to match those in my table in order to print out the Volume Weighted Price.

Next I did some research and found the SQL method DATE FORMAT in order to print the date as DD/MM/YYYY and separate the time from the date. These were then called within the SELECT clause and given aliases DateOnly and TimeOnly. 

I then used BETWEEN/ADD in the WHERE clause to get the Volume Weighted Price between a hardcoded time and 5 hours later. I used the DATE ADD method to find out the 5 hour interval.

When the output from my SQL query was correct,  then put this code inside a Procdure called q1 and added the parameters for the ticker as and date. This allowed me to swap out any hard coded date and times for the parameter indate. 

My code was then tested by saving and calling the prcedure and passing in different arguments.

Lastly, I used Git to push two files to my repository in BitBucket. The first file contained my procedure and the second, a call to my procedure. I did this using the commands:
git add .
git commit -m "some comment"
git push -u origin master


##Challenge Two

For the second challenge, I used the same procedure to import the datasets and begin using the data. I created a new database called challenge 2 and analysed the data.

For the first part of the challenge I calculated the range between the opening and closing prices then used the ORDER BY clause descending on date and set a limit to get the biggest range for the top three dates. This was then saved as a temporary table.

I then created a sql statement to get the max price per each date. I used the aggrogate function MAX(high) to get the maximum high price, then used group by date in order to get the max price for each date with the biggest range. A subquery within the where clause allowed me to use the dates from the temporary table DaysByLargestRange. This was then stores as a temporary table.

In order to find the time at each of the max prices, I used an inner join to join the original table to get the times with the MaxPricePerDate temporary table.

The last temporary table DateRangeTime was created by using inner join to get the dates and ranges from DaysByLargestRange and the time from TimeAtMaxPrice. 

When all the queries were complete and the correct values for Date, Range and TimeAtMaxPrice were correct, they were saved within a procedure (q2) and pushed to my git repository.
